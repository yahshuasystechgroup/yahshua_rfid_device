# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-09-22 05:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rfid_app', '0006_remove_log_notification_scan_type'),
    ]

    operations = [
        migrations.CreateModel(
            name='Special_log_notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rfid', models.CharField(blank=True, max_length=50, null=True)),
                ('is_notified', models.BooleanField(default=False)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'special_log_notification',
            },
        ),
    ]
