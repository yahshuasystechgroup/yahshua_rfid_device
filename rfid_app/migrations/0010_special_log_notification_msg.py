# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-12-26 05:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rfid_app', '0009_auto_20180925_1111'),
    ]

    operations = [
        migrations.AddField(
            model_name='special_log_notification',
            name='msg',
            field=models.TextField(blank=True, null=True),
        ),
    ]
