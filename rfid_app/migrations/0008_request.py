# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-09-22 07:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rfid_app', '0007_special_log_notification'),
    ]

    operations = [
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('request_type', models.CharField(blank=True, max_length=50, null=True)),
                ('is_active', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'requests',
            },
        ),
    ]
