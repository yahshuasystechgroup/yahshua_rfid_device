from django.db import models

class Employees(models.Model):
	employee_id = models.IntegerField()
	employee_name = models.TextField()
	employee_rfid = models.CharField(max_length=50,blank=True, null=True)
	chapa = models.IntegerField(blank=True, null=True)

	log_type = models.TextField(blank=True,null=True)
	date = models.DateField(blank=True,null=True)
	log_time = models.TimeField(blank=True,null=True)

	class Meta:
		app_label = "rfid_app"
		db_table  = "employees"

	def get_dict(self):
		row = {
			"employee_id": self.employee_id,
			"employee_name": self.employee_name,
			"employee_rfid": self.employee_rfid,
			"chapa": self.chapa,
			"log_type": self.log_type,
			"date": self.date,
			"log_time": self.log_time,
		}
		return row

class Special_rfid(models.Model):
	rfid = models.CharField(max_length=50,blank=True, null=True)
	description = models.CharField(max_length=50,blank=True, null=True)
	id_type = models.CharField(max_length=50,blank=True, null=True)

	class Meta:
		app_label = "rfid_app"
		db_table  = "special_rfids"

class Time_log(models.Model):
	rfid = models.CharField(max_length=50,blank=True, null=True)
	log_type = models.TextField()
	company = models.IntegerField()
	employee_id = models.IntegerField(blank=True,null=True)
	date = models.DateField(verbose_name = "Date")
	log_time = models.TimeField(blank=True,null=True)
	img_path = models.TextField(blank=True,null=True)
	sync_id = models.TextField(blank=True,null=True)
	live_sync_id = models.TextField(blank=True,null=True)
	is_sync = models.BooleanField(default = False)
	is_sync_live = models.BooleanField(default = False)
	device_id = models.TextField()
	ip = models.CharField(max_length = 15,null = True)

	# is_notified = models.BooleanField(default = False)

	class Meta:
		app_label = "rfid_app"
		db_table  = "time_logs"

	def get_dict(self):
		row = {
			"id": self.id,
			"rfid": self.rfid,
			"log_type": self.log_type,
			"employee_id": self.employee_id,
			"date": self.date,
			"log_time": self.log_time,
		}
		return row
		


class Unregistered_rfid(models.Model):
	rfid = models.CharField(max_length=50,blank=True, null=True)
	date = models.DateField(verbose_name = "Date")
	time = models.TimeField(blank=True,null=True)
	img_path = models.TextField(blank=True,null=True)
	sync_id = models.TextField(blank=True,null=True)
	device_id = models.TextField()
	ip = models.CharField(max_length = 15,null = True)
	# is_notified = models.BooleanField(default = False)

	class Meta:
		app_label = "rfid_app"
		db_table  = "unregistered_rfid"


class Timelog_error(models.Model):
	rfid = models.CharField(max_length=50)
	log_type = models.TextField(blank=True,null=True)
	company = models.IntegerField(blank=True,null=True)
	employee = models.IntegerField(blank=True,null=True)
	date = models.DateField(verbose_name = "Date")
	time = models.TimeField()
	error_msg = models.TextField(null=True)
	created_on = models.DateTimeField(auto_now_add = True)
	is_sync = models.BooleanField(default = False)
	device_id = models.TextField(blank=True,null=True)
	ip = models.CharField(max_length = 15,null = True)

	class Meta:
		app_label = "rfid_app"
		db_table = "timelog_errors"

	def get_dict(self):
		# row = model_to_dict(self, fields = [
		# 	"rfid",
		# 	"employee",
		# 	"date",
		# 	"time",
		# 	"error_msg",
		# 	"is_sync"
		# ])

		row = {
			"id": self.id,
			"rfid": self.rfid,
			"employee": self.employee,
			"date": self.date,
			"time": self.time,
			"error_msg": self.error_msg,
			"is_sync": self.is_sync,
		}
		return row


class Log_type_setting(models.Model):
	is_log_in = models.BooleanField(default = True)
	rfid = models.CharField(max_length=50,blank=True, null=True)
	created_on = models.DateTimeField(auto_now_add = True)

	class Meta:
		app_label = "rfid_app"
		db_table = "log_type_settings"

	def get_dict(self):
		row = model_to_dict(self, fields = [
			"id",
			"is_log_in",
			"rfid",
			"created_on",
		])
		return row


class Log_notification(models.Model):
	rfid = models.CharField(max_length=50,blank=True, null=True)
	log = models.ForeignKey("Time_log", blank=True, null=True)
	error_log = models.ForeignKey("Unregistered_rfid", blank=True, null=True)
	msg = models.TextField(blank=True, null=True)
	chapa = models.IntegerField(blank=True, null=True)
	# scan_type = models.CharField(max_length=50,blank=True, null=True)
	is_notified = models.BooleanField(default = False)
	created_on = models.DateTimeField(auto_now_add = True)

	class Meta:
		app_label = "rfid_app"
		db_table = "log_notification"

	def get_dict(self):	
		row = model_to_dict(self, fields = [
			"id",
			"rfid",
			"msg",
			# "scan_type",
			"is_notified",
			"created_on",
		])
		row["error_log"] = self.error_log.get_dict() if self.error_log else None
		row["log"] = self.log.get_dict() if self.log else None

		return row

	def change_status(self):
		self.is_notified = True
		self.save()


class Special_log_notification(models.Model):
	rfid = models.CharField(max_length=50,blank=True, null=True)
	msg = models.TextField(blank=True, null=True)
	# scan_type = models.CharField(max_length=50,blank=True, null=True)
	is_notified = models.BooleanField(default = False)
	created_on = models.DateTimeField(auto_now_add = True)

	class Meta:
		app_label = "rfid_app"
		db_table = "special_log_notification"

	def get_dict(self):	
		row = model_to_dict(self, fields = [
			"id",
			"rfid",
			# "msg",
			# "scan_type",
			"is_notified",
			"created_on",
		])

		return row

	def change_status(self):
		self.is_notified = True
		self.save()

class Request(models.Model):
	request_type = models.CharField(max_length=50,blank=True, null=True)
	is_active = models.BooleanField(default = False)

	class Meta:
		app_label = "rfid_app"
		db_table = "requests"

	def change_status(self, status):
		self.is_active = status
		self.save()
