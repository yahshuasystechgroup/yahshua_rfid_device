from django.apps import AppConfig


class RfidAppConfig(AppConfig):
    name = 'rfid_app'
