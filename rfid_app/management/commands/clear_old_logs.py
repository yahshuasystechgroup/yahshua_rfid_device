from django.core.management.base import BaseCommand
from rfid_app.views.sync import *

class Command(BaseCommand):
	help = "This is my test command"
	def handle(self, *args, **options): #orig
		self.stdout.write("SYNCING EMPLOYEES...")
		clear_old_logs()