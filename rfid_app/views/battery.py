from ..views.common import *

import struct
import smbus
import sys


GPIO.setup(6,GPIO.IN) 
state_charge = (3.622, 3.832, 4.043, 4.182, 4.21)
state_discharge = (4.17, 3.74751, 3.501, 3.35, 2.756)
state_charging = False;
v_current = 0;
v_old = 0;
capacity = 0;

def read_voltage(bus):

	"This function returns as float the voltage from the Raspi UPS Hat via the provided SMBus object"
	address = 0x36
	read = bus.read_word_data(address, 2)
	swapped = struct.unpack("<H", struct.pack(">H", read))[0]
	voltage = swapped * 78.125 /1000000
	return voltage


def read_capacity(bus):
	"This function returns as a float the remaining capacity of the battery connected to the Raspi UPS Hat via the provided SMBus object"
	address = 0x36
	read = bus.read_word_data(address, 4)
	swapped = struct.unpack("<H", struct.pack(">H", read))[0]
	capacity = swapped/256
	return capacity


def battery_charge(): 
	bus = smbus.SMBus(1)  # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)

	power_source = GPIO.input(6) == 1


	v_old = read_voltage(bus)
	v_current = v_old

	if (v_current > state_charge[4]):
		capacity = 100
	elif ((v_current < state_charge[4]) and (v_current >= state_charge[3])):
		capacity = (v_current - state_charge[3]) / ((state_charge[4] - state_charge[3]) / 25) + 75
	elif ((v_current < state_charge[3]) and (v_current >= state_charge[2])):
		capacity = (v_current - state_charge[2]) / ((state_charge[3] - state_charge[2]) / 25) + 50
	elif ((v_current < state_charge[2]) and (v_current >= state_charge[1])):
		capacity = (v_current - state_charge[1]) / ((state_charge[2] - state_charge[1]) / 25) + 25
	elif ((v_current < state_charge[1]) and (v_current >= state_charge[0])):
		capacity = (v_current - state_charge[0]) / ((state_charge[1] - state_charge[0]) / 25)
	else:
		capacity = 0
		
	status = 100
	batt = round(read_capacity(bus))
	if batt >= 92:
		status = 100
	elif batt < 92 and batt >= 85:
		status = 90
	elif batt < 85 and batt >= 75:
		status = 75
	elif batt < 75 and batt >= 60:
		status = 60
	elif batt < 60 and batt >= 45:
		status = 45
	elif batt < 45 and batt >= 30:
		status = 30
	elif batt < 30 and batt >= 15:
		status = 15
	elif batt < 15:
		status = 0


	#print "Voltage:%5.2fV" % read_voltage(bus)
	#print "Battery:%5i%%" % capacity
	#print "Indicator:%5i%%" % status

	# draw battery

	n = int(round(read_capacity(bus) / 10));
	#print "----------- "
	#print("charge")
	#print(n)

	#sys.stdout.write('|')

	#for i in range(0,n):

	#	sys.stdout.write('#')

	#for i in range(0,10-n):

	#	sys.stdout.write(' ')

	#sys.stdout.write('|+\n')

	#print "----------- "


	#if read_capacity(bus) == 100:

	#	print "Battery FULL"

	#if read_capacity(bus) < 20:

	#	print "Battery LOW"

	
	#if batt >= 92:
		#batt_percentage = 100	
	#elif batt < 92 and batt >= 90:
		#batt_percentage = 95
	#elif batt < 90 and batt >= 85:
		#batt_percentage = 90
	#else:
		#batt_percentage = batt
	
	#for i in range(10)
	if batt > 90:
		batt_percentage = 100
	elif batt <= 90 and batt >= 80:
		batt_percentage = batt + (10 - (90 - batt))
	else:
		batt_percentage = batt

	#return False, status
	#return power_source, status, batt
	return power_source, status, batt_percentage

#def battery_discharge(): 
def battery_charge_orig(): 
 	bus = smbus.SMBus(1)  # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)
	power_source = GPIO.input(6) == 1
 	v_old = read_voltage(bus)
 	v_current = v_old

 	if (v_current > state_discharge[0]):
 		capacity = 100
 	elif ((v_current < state_discharge[0]) and (v_current >= state_discharge[1])):
 		capacity = (state_discharge[0] - v_current) /((state_discharge[0] - state_discharge[1]) / 33) + 75
 	elif ((v_current < state_discharge[1]) and (v_current >= state_discharge[2])):
 		capacity = (state_discharge[1] - v_current) / ((state_discharge[1] - state_discharge[2]) / 33) + 50
 	elif ((v_current < state_discharge[2]) and (v_current >= state_discharge[3])):
 		capacity = (state_discharge[2] - v_current) / ((state_discharge[2] - state_discharge[3]) / 25) + 25
 	elif ((v_current < state_discharge[3]) and (v_current >= state_discharge[4])):
 		capacity = (state_discharge[3] - v_current) / ((state_discharge[3] - state_discharge[4]) / 25)
 	else:
 		capacity = 0
 		
	status = 100
	batt = round(read_capacity(bus))
	if batt >= 90:
		status = 100
	elif batt < 90 and batt >= 80:
		status = 90
	elif batt < 80 and batt >= 75:
		status = 75
	elif batt < 75 and batt >= 60:
		status = 60
	elif batt < 60 and batt >= 45:
		status = 45
	elif batt < 45 and batt >= 30:
		status = 30
	elif batt < 30 and batt >= 15:
		status = 15
	elif batt < 15:
		status = 0

  	#print "Voltage:%5.2fV" % read_voltage(bus)
  	#print "Battery:%5i%%" % capacity
	# print "Indicator:%5i%%" % status

  	# draw battery

  	n = int(round(read_capacity(bus) / 10));
	#print(round(read_capacity(bus)))
  	#print "----------- "
  	#print("discharge")
 	

  	#sys.stdout.write('|')

  	#for i in range(0,n):

  	#	sys.stdout.write('#')

  	#for i in range(0,10-n):

  	#	sys.stdout.write(' ')

  	#sys.stdout.write('|+\n')

  	#print "----------- "


 # 	if read_capacity(bus) == 100:

 # 		print "Battery FULL"

 # 	if read_capacity(bus) < 20:

 # 		print "Battery LOW"
 	
 	#return False, status
	return power_source, status, batt
