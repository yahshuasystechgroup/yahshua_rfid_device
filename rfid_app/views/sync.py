LIVE_URL = "https://delmontepayroll.com/"
URL = "http://192.168.25.95" #local timekeeper_biometrics
# URL = "http://192.168.22.9:9000" #local timekeeper_biometrics
DEVICE_ID = "DMPI-001"

# import os
# import requests
from ..views.common import *

from ..models.rfid import *
from ..forms.rfid import *
from requests.exceptions import Timeout
from django.core.exceptions import MultipleObjectsReturned

import json
import os

def sync():


	#AUTHORIZED API CONNECTION
	# authorization = requests.get(URL+'/api/api-auth/', data={'username':'payroll.systech@gmail.com','password':'bypasses3223'})
	# GET LOG SYNC SCHEDULE
	# cur.execute("SELECT sync_time FROM sync_schedule")
	# device_sync_schedules = cur.fetchall()
	# current_time = datetime.now().time().hour

	# if current_time == 2 or current_time == 16:
	# 	sync_employees()
	# 	print("%s - EMPLOYEES SYNC SUCCESSFUL! (%s)"%(DEVICE_ID,datetime.now()))

	# if current_time % 16 == 0:
	# 	sync_schedules()
	# 	print("%s - SCHEDULES SYNC SUCCESSFUL! (%s)"%(DEVICE_ID,datetime.now()))


	# for s_sched in device_sync_schedules:
		
	# 	sync_time = s_sched[0].hour
	# 	print(sync_time)
	# 	print(current_time)
	# 	if sync_time == current_time:
	# sync_employees2()
	sync_logs()
	sync_error_logs()
# FOR TESTING PURPOSE ONLY
#sync_employees(cur)
#sync_schedules(cur)
#sync_logs(cur)

#CONVERT IMAGE TO BASE64
def image_encoder(fname):
	encoded_string=""
	with open(fname, "rb") as image_file:
		if image_file:
			encoded_string = base64.b64encode(image_file.read())
		else:
			encoded_string = None
	return encoded_string

#GET ALL CURRENT EMPLOYEES IN THE DEVICE
def get_device_employees():
	result = Employees.objects.all().values_list("employee_id", flat = True)
	return result

#SYNCS ALL ERROR LOGS
def sync_error_logs():
	try:
		instances = Timelog_error.objects.filter(is_sync = False)
		if instances.exists():
			ipv4 = None
			try:
				ipv4 = os.popen('ip addr show wlan0').read().split("inet ")[1].split("/")[0]
			except Exception as e:
				pass
				
			try:
				ipv4 = os.popen('ip addr show eth0').read().split("inet ")[1].split("/")[0]
			except Exception as e:
				pass

			results = []
			for instance in instances:
				row = instance.get_dict()
				row["ip_address"] = ipv4
				row["date"] = str(instance.date)
				row["time"] = str(instance.time)
				results.append(row) 

			headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

			#BATCH SYNCING BY 10 
			data_for_syncing = []
			log_count = 0
			error_data = []
			success_data = []
			for i,res in enumerate(results):
				
				if (i+1) % 10 == 0:
					sync_data = requests.post(URL+'/api/sync-error-log/', data=json.dumps(data_for_syncing), headers=headers, timeout=2)
					response = json.loads(sync_data._content)
					# error_data += response.get("error_data", [])
					success_data = success_data + response.get("success_data", [])
					if sync_data.status_code == 200:
						# set_sync_complete(data_for_syncing)
						log_count += 10
					data_for_syncing = []
				else:
					data_for_syncing.append(res)
					if len(results) == (i + 1):
						sync_data = requests.post(URL+'/api/sync-error-log/', data=json.dumps(data_for_syncing), headers=headers, timeout=2)
						response = json.loads(sync_data._content)
						# error_data += response.get("error_data", [])
						success_data += response.get("success_data", [])
						if sync_data.status_code == 200:
							# set_sync_complete(data_for_syncing)
							log_count += len(data_for_syncing)

			set_sync_error_complete(success_data)

		return True
	except requests.ConnectionError, e:
		print("Connection failure : " + str(e))
		return True

	except Exception as e:
		print(str(e))
		return True


#RETRIEVE UNSYNCED DATA FROM THE DEVICE	
def get_unsynced_data(direct = False, return_count = False):
	'''
		Sample Parameters needed:
			date: "2018-02-23",
			emp_name: "Catherine Jean L. Nocete",
			employee: 452,
			is_sync: true,
			log_img: "base 64 img here",
			log_time: "17:36",
			log_type: "IN",
			sync_id: 1513978613581
	'''
	unsynced_data = Time_log.objects.filter(is_sync = False)
	if return_count:
		return unsynced_data.count()
	# unsynced_data = Time_log.objects.all()
	
	for_synching = []
	for data in unsynced_data:

		# encoded_string = image_encoder(data.img_path)
		if data.employee_id:
			employee_details = Employees.objects.filter(employee_id = data.employee_id)
		else:
			employee_details = Employees.objects.filter(employee_rfid = data.rfid)


		if employee_details.exists():
			employee_detail = employee_details.first()
			log_time = data.log_time.strftime("%H:%M:%S.%f")
			#COMBINE LOG DATE AND LOG TIME FOR SYNC ID
			# combined = datetime.combine(datetime.strptime(str(data.date), "%Y-%m-%d") ,datetime.strptime(str(data.log_time),"%H:%M:%S.%f").time()).strftime('%s')
			try:
				timestamp = datetime.combine(datetime.strptime(str(data.date), "%Y-%m-%d") ,datetime.strptime(str(data.log_time),"%H:%M:%S.%f").time())
			except Exception as e:
				timestamp = datetime.combine(datetime.strptime(str(data.date), "%Y-%m-%d") ,datetime.strptime(str(data.log_time),"%H:%M:%S").time())
			
			row = {
				"log_id": data.id,
				"date": str(data.date),
				"emp_name": employee_detail.employee_name,
				"employee": employee_detail.employee_id,
				# "is_sync": True,
				# "log_img": encoded_string,
				# "log_img": None,
				"log_time": log_time,
				"log_type": data.log_type,
				"sync_id": int(timestamp.strftime('%s'))*1000
			}
			if not direct:
				row["timestamp"] = str(timestamp)
				row["ip"] = data.ip
			for_synching.append(row)
	return for_synching

# SET SYNC_ID FOR LOGS ON SUCCESSFUL SYNC (BY BATCH)		
def set_sync_complete_old(synced_data):
	for data in synced_data:
		try:
			log = Time_log.objects.get(id = data["log_id"])
			log.sync_id = data["sync_id"]
			log.save()
		except Time_log.DoesNotExist:
			continue

def set_sync_complete(synced_data, live = False):
	logs = Time_log.objects.filter(id__in = synced_data)
	for log in logs:
		if live:
			log.is_sync_live = True
			log.is_sync = True
		else:
			log.is_sync = True
		log.save()

def set_sync_error_complete(synced_data, live = False):
	logs = Timelog_error.objects.filter(id__in = synced_data)
	for log in logs:
		log.is_sync = True
		log.save()


# GET DEVICE SYNC SCHEDULE FOR LOGS FROM LIVE
# def sync_schedules():
# 	sync_schedule = requests.post(URL+'/api/device-sync-schedule/', headers={'Authorization':'token 6747ed5431fbfd27208711483fe6dc4aa2e502c5'}, data={"device_id" : "DMPI-001"})
# 	schedules = sync_schedule.json()
# 	cur.execute("DELETE FROM sync_schedule")
# 	for sched in schedules:
# 		cur.execute("INSERT INTO sync_schedule (sync_time) VALUES ('%s')"%(sched))
# 		conn.commit()

def sync_employees():
	# GET ALL EMPLOYEES IN THE DEVICE FIRST
	current_device_employees = get_device_employees()

	

	#SYNC DEVICE EMPLOYEES (INSERT/DELETE/UPDATE)
	# api_data = requests.post(URL+'/api/device-employees/', headers={'Authorization':'token 6747ed5431fbfd27208711483fe6dc4aa2e502c5'}, data={"device_id" : "DMPI-001"})
	# api_data = requests.post(URL+'/api/device-employees/', headers={'Authorization':'token 666a460729a6d0792c42dd1fae88dc0a0aed49ae'}, data={"device_id" : "DMPI-001"},timeout=1)
	api_data = requests.post(URL+'/api/device-employees/', headers={'Authorization':'token 6c0e7ae3572769bf054a17c538af0819db723509'}, data={"device_id" : "DMPI-001"},timeout=1)
	data = api_data.json()

	#GET ALL IDS OF EMPLOYEES FROM LIVE
	synced_employees = []
	for row in data:
		synced_employees.append(row["employee_id"])

	#GET EMPLOYEE IDS FOR DELETION
	new_employees = list(set(synced_employees) & set(current_device_employees)) + list(set(synced_employees) - set(current_device_employees))
	employees_to_delete = list(set(current_device_employees) - set(new_employees))

	# UPDATE EXISTING AND INSERT NON EXISTING EMPLOYEES
	for res in data:
		try:
			instance = Employees.objects.get(id=res.get('employee_id',None))
			employees_form = Employees_form(res, instance=instance)
			editing = True
		except Employees.DoesNotExist:
			employees_form = Employees_form(res)

		if(employees_form.is_valid()):
			employees_save = employees_form.save()

	# DELETE FROM EMPLOYEES FROM DEVICE IF NOT IN SYNC 
	Employees.objects.filter(id__in = employees_to_delete).delete()

def sync_employees2(auto = False):
	try:
		ip = os.popen('ip addr show eth0').read().split("inet ")[1].split("/")[0]
		# ip = os.popen('ip addr show wlan0').read().split("inet ")[1].split("/")[0]

		ddata = {
			"ip": ip
		}
		ddata["check"] = auto 
		api_data = requests.post(URL+'/api/get-employees/', data = ddata)
		data = json.loads(api_data.content)
		
		# data = api_data.json()
		# if api_data.status_code != 200:
		# 	raise_error(api_data._content)

		current_device_employees = get_device_employees()
		# data = api_data.data

		#GET ALL IDS OF EMPLOYEES FROM LIVE
		synced_employee_ids =  []
		synced_employees = {}
		for row in data["data"]:
			synced_employee_ids.append(row["employee_id"])
			synced_employees[row["employee_id"]] = row

		#GET EMPLOYEE IDS FOR DELETION
		new_employees = list(set(synced_employee_ids) & set(current_device_employees)) + list(set(synced_employee_ids) - set(current_device_employees))
		employees_to_delete = list(set(current_device_employees) - set(new_employees))
		# UPDATE EXISTING AND INSERT NON EXISTING EMPLOYEES

		new_data = {}
		new_data_ids = []
		for_entry = []
		for row in synced_employees:
			if row in current_device_employees:
				new_data[row] = synced_employees[row]
				new_data_ids.append(row)
			else:
				for_entry.append(synced_employees[row])
		all_instances = Employees.objects.filter(employee_id__in = new_data_ids).values("id","employee_id","employee_rfid","chapa")

		for_update = []
		log_updates = []
		

		for row in all_instances:
			checker = new_data.get(row["employee_id"], None)
			if checker:
				if new_data[row["employee_id"]]["employee_rfid"] != row["employee_rfid"] or new_data[row["employee_id"]]["employeeid"]!= row["chapa"]:
					for_update.append(new_data[row["employee_id"]])

				if  new_data[row["employee_id"]]["log_type"] or new_data[row["employee_id"]]["date"] or new_data[row["employee_id"]]["log_time"]:
					log_updates.append(new_data[row["employee_id"]])
			# else:
			# 	for_entry.append(new_data[row["employee_id"]])
		for row in for_update:
			Employees.objects.filter(employee_id = row["employee_id"]).update(employee_rfid = row["employee_rfid"], chapa = row["employeeid"])

		#update to latest employee_log
		for row in log_updates:
			Employees.objects.filter(employee_id = row["employee_id"]).update(log_type = row["log_type"], date = row["date"], log_time=row["log_time"])



		for row in for_entry:
			row["chapa"] = row["employeeid"]
			employees_form = Employees_form(row)
			if(employees_form.is_valid()):
				employees_save = employees_form.save()
			# Employees.objects.filter(employee_id = row["employee_id"]).update(employee_rfid = row["employee_rfid"], chapa = row["chapa"])

		# for res in new_data:
		# 	if "employeeid" in res:
		# 		res["chapa"] = res["employeeid"]
		# 	try:
		# 		instance = Employees.objects.get(employee_id=res.get('employee_id',None))
		# 		if instance.employee_rfid != res["employee_rfid"] or instance.chapa != res["chapa"]:
		# 			employees_form = Employees_form(res, instance=instance)
		# 		else:
		# 			continue
		# 		editing = True
		# 	except Employees.DoesNotExist:
		# 		employees_form = Employees_form(res)

		# 	if(employees_form.is_valid()):
		# 		employees_save = employees_form.save()

		# DELETE FROM EMPLOYEES FROM DEVICE IF NOT IN SYNC 
		Employees.objects.filter(employee_id__in = employees_to_delete).delete()

		# if auto:
		# 	return {"ids": new_employees, "success": True}

	except Exception as e:
		return True

def auto_sync_logs():
    try:
        _ = requests.get(URL, timeout = .5)
        sync_logs()
        print("Local Server Connection Status : UP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    except requests.ConnectionError:
        print("Local Server Connection Status : DOWN >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        # print("No internet connection available.")
    except requests.exceptions.Timeout:
		print("Timeout occured.")
    return False

def sync_logs():
	#GET UNSYNCED DATA IN DEVICE
	try:
		result = get_unsynced_data()
		
		if not result:
			print("%s HAVE NO LOGS TO SYNC! - (%s)"%(DEVICE_ID, datetime.now()))
			return

		# SET HEADERS FOR REQUEST
		# headers = {'Authorization':'Token 666a460729a6d0792c42dd1fae88dc0a0aed49ae', 'Content-Type': 'application/json', 'Accept': 'application/json'}
		headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

		#BATCH SYNCING BY 10 
		data_for_syncing = []
		log_count = 0
		error_data = []
		success_data = []
		for i,res in enumerate(result):
			
			if (i+1) % 10 == 0:
				sync_data = requests.post(URL+'/api/sync-time-in-out/', data=json.dumps(data_for_syncing), headers=headers, timeout=2)
				response = json.loads(sync_data._content)
				error_data += response.get("error_data", [])
				success_data = success_data + response.get("success_data", [])
				if sync_data.status_code == 200:
					# set_sync_complete(data_for_syncing)
					log_count += 10
				data_for_syncing = []
			else:
				data_for_syncing.append(res)
				if len(result) == (i + 1):
					sync_data = requests.post(URL+'/api/sync-time-in-out/', data=json.dumps(data_for_syncing), headers=headers, timeout=2)
					response = json.loads(sync_data._content)
					error_data += response.get("error_data", [])
					success_data += response.get("success_data", [])
					if sync_data.status_code == 200:
						# set_sync_complete(data_for_syncing)
						log_count += len(data_for_syncing)

		set_sync_complete(success_data)
		# for synced in success_data:

		print("LOGS SYNC SUCCESSFUL! - (%s - %d log(s) synced)"%(datetime.now(), log_count))
		return True
	except requests.ConnectionError, e:
		print "Connection failure : " + str(e)
		return True
	except Exception as e:
		# raise ValueError(e)
		print(str(e))
		return True

def get_last_log(employee_rfid,current_datetime):

	data = {}
	COOLDOWN_TIME = 4
	try:
		employee = Employees.objects.get(employee_rfid = employee_rfid)
		last_log_minute = None
		if employee.date and employee.log_time:
			last_log =  datetime.combine(employee.date ,employee.log_time)
			last_log_seconds = (current_datetime - last_log).total_seconds()
			last_log_minute = last_log_seconds / 60
		if last_log_minute and last_log_minute <= COOLDOWN_TIME:
			return employee.get_dict()

		try:
			# api_data = requests.post(URL+'/api/last-log/', headers={'Authorization':'token 666a460729a6d0792c42dd1fae88dc0a0aed49ae'}, data={"employee_id" : employee.employee_id},timeout=0.5)
			api_data = requests.post(URL+'/api/last-log/', headers={'Authorization':'token 6c0e7ae3572769bf054a17c538af0819db723509'}, data={"employee_id" : employee.employee_id},timeout=0.8)
			if api_data.status_code == 200:
				data = api_data.json()
				if date.get("date",None):
					data["date"] = datetime.strptime(str(data["date"]), "%Y-%m-%d")
				if date.get("log_time", None):
					data["log_time"] = datetime.strptime(str(data["log_time"]),"%H:%M:%S.%f").time()
		except Exception as e:
				data = employee.get_dict()

	except MultipleObjectsReturned:
		employee = Employees.objects.filter(employee_rfid = employee_rfid).last()

		last_log_minute = None
		if employee.date and employee.log_time:
			last_log =  datetime.combine(employee.date ,employee.log_time)
			last_log_seconds = (current_datetime - last_log).total_seconds()
			last_log_minute = last_log_seconds / 60
		if last_log_minute and last_log_minute <= COOLDOWN_TIME:
			return employee.get_dict()

		try:
			# api_data = requests.post(URL+'/api/last-log/', headers={'Authorization':'token 666a460729a6d0792c42dd1fae88dc0a0aed49ae'}, data={"employee_id" : employee.employee_id},timeout=0.5)
			api_data = requests.post(URL+'/api/last-log/', headers={'Authorization':'token 6c0e7ae3572769bf054a17c538af0819db723509'}, data={"employee_id" : employee.employee_id},timeout=0.8)
			if api_data.status_code == 200:
				data = api_data.json()
				if date.get("date",None):
					data["date"] = datetime.strptime(str(data["date"]), "%Y-%m-%d")
				if date.get("log_time", None):
					data["log_time"] = datetime.strptime(str(data["log_time"]),"%H:%M:%S.%f").time()
		except Exception as e:
				data = employee.get_dict()

	except Employees.DoesNotExist:
		data = {}
		
	return data

def get_local_last_log(employee_rfid):
	last_log = Time_log.objects.filter(rfid = employee_rfid).order_by("-date", "-log_time")
	row = None
	if last_log.exists():
		row = last_log.first()
		row = row.get_dict()

	return row


def direct_sync():
	try:
		result = get_unsynced_data(True)
		
		if not result:
			return ({
				"success": "False",
				"msg": "NO LOGS TO SYNC"
			})
			# print("NO LOGS TO SYNC! - (%s)"%(datetime.now()))
			return

		# SET HEADERS FOR REQUEST
		headers = {'Authorization':'Token 6747ed5431fbfd27208711483fe6dc4aa2e502c5', 'Content-Type': 'application/json', 'Accept': 'application/json'}
		# headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

		#BATCH SYNCING BY 10 
		data_for_syncing = []
		log_count = 0
		error_data = []
		success_data = []
		for i,res in enumerate(result):
			
			if (i+1) % 10 == 0:
				sync_data = requests.post(LIVE_URL+'/api/sync-time-in-out/', data=json.dumps({"from_rfid": True,"log_list": data_for_syncing}), headers=headers)
				response = json.loads(sync_data._content)
				error_data += response.get("error_data", [])
				success_data = success_data + response.get("success_data", [])
				if sync_data.status_code == 200:
					# set_sync_complete(data_for_syncing)
					log_count += 10
				data_for_syncing = []
			else:
				data_for_syncing.append(res)
				if len(result) == (i + 1):
					sync_data = requests.post(LIVE_URL+'/api/sync-time-in-out/', data=json.dumps({"from_rfid": True,"log_list": data_for_syncing}), headers=headers)
					response = json.loads(sync_data._content)
					error_data += response.get("error_data", [])
					success_data += response.get("success_data", [])
					if sync_data.status_code == 200:
						# set_sync_complete(data_for_syncing)
						log_count += len(data_for_syncing)
		set_sync_complete(success_data, True)
		# for synced in success_data:
		return ({
				"success": "True",
				"msg": "SYNC SUCCESSFUL"
			})
		print("LOGS SYNC SUCCESSFUL! - (%s - %d log(s) synced)"%(datetime.now(), log_count))
	except Exception as e:
		# raise ValueError(e)
		return ({
				"success": "False",
				"msg": "SYNC ERROR"
			})

#GET CURRENT DEVICE STATUS
def get_device_status():
	ipv4 = "0.0.0.0"
	headers = {'Authorization':'Token 6747ed5431fbfd27208711483fe6dc4aa2e502c5', 'Content-Type': 'application/json', 'Accept': 'application/json'}
	try:
		ipv4 = os.popen('ip addr show wlan0').read().split("inet ")[1].split("/")[0]
	except Exception as e:
		pass
		
	try:
		ipv4 = os.popen('ip addr show eth0').read().split("inet ")[1].split("/")[0]
	except Exception as e:
		pass

	params = {
		"ip_address": ipv4,
	}

	if ipv4 != "0.0.0.0":
		requests.post(URL+'/api/device-status/', data=json.dumps(params), headers=headers)

def clear_old_logs():
	""" Clear 3 months old synced time logs and Notification. """
	try:
		from datetime import datetime, timedelta

		timelog_filter = datetime.now()-timedelta(days=90)
		notif_filter = datetime.now()-timedelta(days=1)
		other_log_filter = datetime.now()-timedelta(days=90)
		
		log_notif = Log_notification.objects.filter(created_on__lte=notif_filter, is_notified = True).delete()
		timelog_err = Timelog_error.objects.filter(created_on__lte = other_log_filter, is_sync = True).delete()
		unregistered_ids = Unregistered_rfid.objects.filter(date__lte = other_log_filter, sync_id__isnull = False).delete()
		logs = Time_log.objects.filter(date__lte=timelog_filter, is_sync=True).delete()

		print("-----No. of cleared logs-----")
		print("Notification: %s" %str(log_notif[0]))
		print("Error Logs: %s"%str(timelog_err[0]))
		print("Unregistered id: %s"%str(unregistered_ids[0]))
		print("Logs: %s"%str(logs[0]))
	except Exception as e:
		print(e)




	






