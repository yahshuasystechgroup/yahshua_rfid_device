# from Tones import Tones 
# from HD44780 import HD44780 as lcd

from ..views.common import *
#from ..library.MFRC522 import *
from ..views.tones import *
#from ..views.lcd import *
from ..views.sync import *
from ..models.rfid import *
from ..forms.rfid import *
from django.core.exceptions import MultipleObjectsReturned

import nfc
import ndef
import thread
from nfc.tag import tt1
from nfc.tag import tt2
from nfc.tag import tt3
from nfc.tag import tt4
import os

def main():
	GPIO.setwarnings(False)
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(26,GPIO.OUT)		# Buzzer
	# GPIO.setup(19,GPIO.OUT)	# Camera indicator
	GPIO.setup(13,GPIO.OUT)		# Power Off
	GPIO.setup(12,GPIO.IN) 		# In/Out Status
	GPIO.setup(6,GPIO.IN)		# Power Source
	#GPIO.setup(18,GPIO.OUT)    	# DIM LCD
	#FOR UART RFID
	PortRF = serial.Serial('/dev/ttyS0',9600)
	###

	global temp_log_type
	global current_log_type

	ids = []

	try:
		# global current_log_type;
		current_log_type = "OUT"
		current_path = os.getcwd()
		
		#pwm = GPIO.PWM(18,1000)
		
		
		
		existing_tags = Employees.objects.all().values_list("employee_rfid", flat=True)
		power_special_rfids = get_special_rfids('power')
		settings_special_rfids = get_special_rfids('settings')

		COOLDOWN_TIME = 4
		
		temp_log_type = Value("i",GPIO.input(12))
		
		# p1 = Process(target = change_log_type)
		# time_update = Process(target = update_time)

		#FOR MFRC522
		# reader = SimpleMFRC522()
		###
		ipv4="0.0.0.0"

		try:
			ipv4 = os.popen('ip addr show wlan0').read().split("inet ")[1].split("/")[0]
		except Exception as e:
			pass
			
		try:
			ipv4 = os.popen('ip addr show eth0').read().split("inet ")[1].split("/")[0]
		except Exception as e:
			pass

		clf =  nfc.ContactlessFrontend('tty:S0:pn532')
		# if len(existing_tags) == 0:
		# 	sync()

		#CHECKS RFID ASSIGNMENT FOR POWER AND SETTINGS ACCESS
		while True:
			if len(power_special_rfids) == 0 or len(settings_special_rfids) == 0:
				temp_id=""
				try:
					tag = clf.connect(rdwr={'on-connect': lambda tag: False})
					temp_id = str(tag.identifier).encode('hex').upper()
				except Exception as e:
					continue
				
				ID = ':'.join(temp_id[i:i+2] for i in range(0, len(temp_id), 2))

				buzzer()
				try:
					rfids = Special_rfid.objects.get(rfid = ID)
					Special_log_notification.objects.create(**{"msg": "rfid already assigned."})
					continue
				except Special_rfid.DoesNotExist:
					pass



				row_filter = {
					"rfid__isnull": True
				}

				def_settings = Special_rfid.objects.filter(**row_filter)
				if def_settings.exists():
					def_settings = def_settings.last()
					def_settings.rfid = ID
					def_settings.save()
			else:
				break
		busy = False
		while True:
			
			ID = ""
			employee = {}
			
			tag = None
			temp_id=""
			clf =  nfc.ContactlessFrontend('tty:S0:pn532')
			try:
				tag = clf.connect(rdwr={'on-connect': lambda tag: False})
				temp_id = str(tag.identifier).encode('hex').upper()
			except Exception as e:
				continue
			if busy:
				continue

			ID = ':'.join(temp_id[i:i+2] for i in range(0, len(temp_id), 2))
			 			
			current_datetime = datetime.now()
			log_date = current_datetime.date()
			log_time = current_datetime.time()
			DEVICE_ID = "DMPI_001"
			# ID = str(ID)
			current_time = datetime.now().strftime("%b %d %I:%M %p")
			print("***************************")
			print(ID)
			print(log_date)
			print(log_time)
			print("***************************")
			employee_info = get_employee_info(ID)

			if not employee_info:
				existing_tags = get_all_employee_rfids()

			notify = {
				"rfid": ID,
				"chapa": employee_info["chapa"] if "chapa" in employee_info and employee_info["chapa"]  else 0
			}
			
			if ID in power_special_rfids:  # POWER OFF DEVICE USING RFID
				#if GPIO.input(6) == 0: #POWER OFF VIA RFID CARD IF POWER SOURCE IS ON BATTERY MODE
				GPIO.output(13, True)
				time.sleep(5)
				buzzer()
				continue
			
			if ID in settings_special_rfids: # SETTINGS ACCESS USING RFID
				special_logs = {
					"rfid": ID,
				}
				buzzer()
				try:
					request = Request.objects.get(request_type = "settings")
					if request.is_active:
						create_special_logs(special_logs)
					time.sleep(2)
					continue
				except Request.DoesNotExist:
					time.sleep(2)
					continue
					
			if not ID:
				continue

			if ID not in existing_tags:
				existing_tags = get_all_employee_rfids()

			if ID in existing_tags: # CHECKS IF RFID EXIST ON THE LIST OF EMPLOYEES
				busy = True
				current_log_type = get_status(return_log_type = True)

				get_device_last_log = copy.deepcopy(get_local_last_log(ID))
				if get_device_last_log:
					# device_last_log = datetime.combine(datetime.strptime(str(get_device_last_log["date"]), "%Y-%m-%d") ,datetime.strptime(str(get_device_last_log["log_time"]),"%H:%M:%S.%f").time())
					device_last_log = datetime.combine(get_device_last_log["date"] ,get_device_last_log["log_time"])
					device_last_log_seconds = (current_datetime - device_last_log).total_seconds()
					print("sssssss")
					print((current_datetime - device_last_log).total_seconds())
					print("sssssss")
					device_last_log_minute = device_last_log_seconds / 60
					print("====last log (device start)=====")
					print(get_device_last_log["date"])
					print(get_device_last_log["log_time"])
					print(datetime.combine(get_device_last_log["date"] ,get_device_last_log["log_time"]))
					print("using from device last log: %s"%(str(device_last_log_minute)))
					print("====last log (device end)=====")
					if device_last_log_minute <= COOLDOWN_TIME:
						get_last_log_type = copy.deepcopy(get_device_last_log)
					else:
						print("using from employees last log")
						get_last_log_type = copy.deepcopy(get_last_log(ID, current_datetime))
				else:
					get_last_log_type = copy.deepcopy(get_last_log(ID, current_datetime))
				print("-------------------------")
				print(get_last_log_type)
				last_log_minute = 0
				minutes_from_tap = 0
				last_log_type = "OUT"
				if not get_last_log_type:
					if current_log_type == "OUT":
						last_log_type = "IN"
					else:
						last_log_type = "OUT"
				# online = True
				
				last_log = current_datetime

						
				seconds_from_tap =  0
				if get_last_log_type and get_last_log_type.get("log_type", None):
					last_log_type = get_last_log_type["log_type"]
					last_log_date = get_last_log_type["date"]
					last_log_time = get_last_log_type["log_time"]
					# last_log_time = datetime.strptime(str(get_last_log_type["log_time"]),"%H:%M:%S.%f").time()
					# last_log =  datetime.combine(datetime.strptime(str(get_last_log_type["date"]), "%Y-%m-%d") ,datetime.strptime(str(get_last_log_type["log_time"]),"%H:%M:%S.%f").time())
					last_log =  datetime.combine(get_last_log_type["date"] ,get_last_log_type["log_time"])
					last_log_seconds = (current_datetime - last_log).total_seconds()
					seconds_from_tap = (current_datetime - last_log).seconds
					last_log_minute = last_log_seconds / 60
					minutes_from_tap = seconds_from_tap / 60
					# online = True
				else:
					# online = False
					rows = Time_log.objects.filter(rfid = ID).order_by("-date", "-log_time")
					if rows.exists():
						rows = rows.first()
						last_log_type = rows.log_type
						last_log_time = rows.log_time
						last_log_date = rows.date
						last_log = datetime.combine(rows.date,rows.log_time)
						last_log_seconds = (current_datetime - last_log).total_seconds()
						seconds_from_tap = (current_datetime - last_log).seconds
						last_log_minute = last_log_seconds / 60
						minutes_from_tap = seconds_from_tap / 60
					else:
						last_log_type = current_log_type
						last_log_minute = 60
						last_log = current_datetime - timedelta(days=1)
				
				print("last_log_type before create: %s"%(last_log_type))
				print("current_log_type before create: %s"%(current_log_type))
				print("last_log_minute before create: %s"%(str(last_log_minute)))
				print("-------------------------")
				if last_log_minute > COOLDOWN_TIME:
					#if last_log_type == current_log_type:
					# if (online and last_log_type == current_log_type and last_log_minute < 5):
					# 	time_diff = current_datetime-last_log
					# 	if time_diff.total_seconds() <= 5:
					# 		# buzzer()
					# 		thread.start_new_thread(buzzer,())
					# 		notify["log_type"] = current_log_type
					# 		create_notification("successful",notify)
					# 		busy = False
					# 	else:
					# 		# thread.start_new_thread(buzzer,(False,))
					# 		thread.start_new_thread(buzzer,())
					# 		# buzzer(success=False)
					# 		notify["log_type"] = current_log_type
					# 		create_notification("invalid",notify)
					# 		busy = False	
					# else:
					fname = current_log_type+"_"+ ID + "_" +log_date.strftime("%m-%d-%Y")+"_"+log_time.strftime("%I:%M:%S:%f%p")	
					#log_type = "OUT" if last_log_type == "IN" else "IN"
					log_type = current_log_type
					image_path = settings.MEDIA_ROOT+"/"+fname+".png"
					company = 1
					model = "daily_logs"

					data = {
						"rfid": ID,
						"log_type": log_type, 
						"company": company, 
						"date": log_date, 
						"log_time": log_time, 
						"img_path": image_path, 
						"device_id": DEVICE_ID, 
						"ip": ipv4
					}
					if employee_info and "employee_id" in employee_info:
						data["employee_id"] = employee_info["employee_id"]
					if "chapa" in notify:
						data["chapa"] = notify["chapa"] if "chapa" in notify and notify["chapa"]  else 0
					# buzzer()
					thread.start_new_thread(buzzer,())
					create(data)
					busy = False	
					if ID in existing_tags:
						print "Success! Thank you!"
						# lcd(current_time+"\4"+log_type+" Success!")
						thread.start_new_thread(sync,())
					else:
						print "Access Denied"
					
				else:
					# buzzer(success=False)
					time_diff = current_datetime-last_log
					if time_diff.total_seconds() <= 2:
						# buzzer()
						thread.start_new_thread(buzzer,())
						notify["log_type"] = current_log_type
						create_notification("successful",notify)
						busy = False
					elif time_diff.total_seconds() > 2 and time_diff.total_seconds() <= 6:
						# buzzer(success=False)
						# thread.start_new_thread(buzzer,(False,))
						thread.start_new_thread(buzzer,())
						notify["log_type"] = current_log_type
						create_notification("invalid",notify)
						busy = False
					else:
						thread.start_new_thread(buzzer,())
						if COOLDOWN_TIME - minutes_from_tap == 0:
							notify["time"] = ((COOLDOWN_TIME * 60) + 60)-seconds_from_tap
							notify["unit"] = "sec"
							print("Please try again after %s sec."%(((COOLDOWN_TIME * 60) + 60)-seconds_from_tap))
							create_notification("time_lock", notify)
							#lcd(current_time+"\nPlease try again\4after %s sec."%(((COOLDOWN_TIME * 60) + 60)-last_log_seconds))
						else:
							notify["time"] = COOLDOWN_TIME-minutes_from_tap
							notify["unit"] = "min"
							print("Please try again after %s min."%(COOLDOWN_TIME-minutes_from_tap))
							create_notification("time_lock", notify)
							#lcd(current_time+"\nPlease try again\4after %s min."%(COOLDOWN_TIME-last_log_minute))
						busy = False
						
					
			else:
				existing_tags = Employees.objects.all().values_list("employee_rfid", flat=True)
				power_special_rfids = get_special_rfids('power')
				settings_special_rfids = get_special_rfids('settings')
				fname = str(ID) + "_" +log_date.strftime("%m-%d-%Y")+"_"+log_time.strftime("%I:%M:%S:%f%p")
				image_path = settings.MEDIA_ROOT+"/"+fname+".png"
				data = {
					"rfid" : ID,
					"date" : log_date,
					"time" : log_time,
					"img_path" : image_path,
					"device_id" : DEVICE_ID,
					"ip": ipv4,
				}
				# buzzer(success=False)
				thread.start_new_thread(buzzer,(False,))
				create_unregistered(data)
				error_data = copy.deepcopy(data)
				error_data["error_msg"] = "unregistered"
				busy = False
				print("Unregistered ID. Access Denied!")
			print("***************************")

	except KeyboardInterrupt:
		GPIO.cleanup()

def get_all_employee_rfids():
	records = Employees.objects.all().values_list("employee_rfid", flat=True)	
	return records

def create(data = None):
	try: 
		notify = {
			"rfid": data["rfid"],
			"log_type": data["log_type"]
		}
		if "chapa" in data:
			notify["chapa"] = data["chapa"]
		time_log_form = Time_log_form(data)
		if(time_log_form.is_valid()):
			time_log_save = time_log_form.save()
			if time_log_save:
				notify["log"] = time_log_save
				create_notification("successful",notify)
			return True
		else:
			create_notification("unsuccessful",notify)
			return json.dumps(time_log_form.errors)
	except Exception as err:
		print(err)
		return err

def create_unregistered(data = None):
	try:
		notify = {
			"rfid": data["rfid"],
			"chapa": None
		} 
		unregistered_rfid_form = Unregistered_rfid_form(data)
		if(unregistered_rfid_form.is_valid()):
			unregistered_rfid_save = unregistered_rfid_form.save()
			if unregistered_rfid_save:
				notify["error_log"] = unregistered_rfid_save
				create_notification("unregistered",notify)
			return True
		else:
			create_notification("unregistered",notify)
			return json.dumps(unregistered_rfid_form.errors)
	except Exception as err:
		print(err)
		return err
	

def buzzer(success = True):
	if success:
		GPIO.output(26,True)
		time.sleep(.05)
		GPIO.output(26,False)
	else:
		error_buzz()
			
def do_buzz(duration, interval = 0.5, gpio = 26):
	print("Buzzing for " + str(duration) + "s")
	GPIO.output(gpio, True)
	time.sleep(duration)
	GPIO.output(gpio, False)
	time.sleep(interval)	
	
def get_special_rfids(id_type = None):
	if id_type:
		result = Special_rfid.objects.filter(id_type = id_type, rfid__isnull = False).values_list("rfid", flat=True)
	else:
		return []
	return result

#Triggered thru settings in Kivy
def get_status(return_log_type = True):
	log_type = "IN"
	is_log_in = True
	current = Log_type_setting.objects.all().order_by("-id")
	if current.exists():
		row = current.first()
		is_log_in = row.is_log_in
		if row.is_log_in:
			log_type = "IN"
		else: 	
			log_type = "OUT"

	if not return_log_type:
		return is_log_in
		
	return log_type


def set_status(id_type = "OUT", ID = None):
	log_type = True if id_type == "IN" else False
	
	data = {
		"is_log_in": log_type,
		"rfid": ID
	}

	log_type_setting_form = Log_type_setting_forms(data)
	if log_type_setting_form.is_valid():
		log_type_setting_form.save()
		return success()
	else:
		raise_error(json.dumps(log_type_setting_form.errors))
	
def set_status_old(id_type = "OUT"):
	if id_type == "IN":
		log_type = "OUT"
	else:
		log_type = "IN"

	return log_type

#Triggered using GPIO		
def get_status_old(return_log_type = False):
	current = GPIO.input(12)
	if return_log_type:
		if current == 0:
			log_type = "OUT"
		else: 	
			log_type = "IN"
		return log_type
	return current

def create_notification(scan_type, data):
	has_errors = False
	result = {
		"rfid": data["rfid"]
	}
	if "chapa" in data:
		result["chapa"] = data["chapa"]

	msg = ""
	if scan_type in ["successful", "unsuccessful"]:
		if "log_type" in data:
			msg = "TIME %s %s!"%(data["log_type"],scan_type.upper())
		if "log" in data:
			result["log"] = data["log"]
	elif scan_type == "invalid":
		msg = "ALREADY TIMED %s!"%(data["log_type"])
		has_errors = True
	elif scan_type == "time_lock":
		msg = "TRY AGAIN IN %s %s."%(str(data["time"]),data["unit"])
		has_errors = True
	else:
		if "error_log" in data:
			result["error_log"] = data["error_log"]
		msg = "ID IS NOT REGISTERED"
		has_errors = True

	result["msg"] = msg
	
	if has_errors:
		emp_info = get_employee_info(data["rfid"])
		error_data = {
			"rfid": data["rfid"],
			"employee":emp_info.get("employee_id"),
			"date": datetime.now().date(),
			"time": datetime.now().time(),
			"error_msg": "time_lock" if scan_type == "invalid" else scan_type
		}
		thread.start_new_thread(create_error_logs,(error_data,))

	try:
		log_notifications = Log_notification.objects.create(**result)
	except Exception as e:
		print(e)

def create_error_logs(data):
	try:
		timelog_error_form = Timelog_error_form(data)
		timelog_error_form.errors
		if timelog_error_form.is_valid():
			timelog_error_form.save()
	except Exception as e:
		print(e)



def create_special_logs(data):
	try:
		log_notifications = Special_log_notification.objects.create(**data)
	except Exception as e:
		print(e)

def battery_indicator():
	# if all zero - battery full
	batt_status = 0
	#if GPIO.input(1) == 1:
	#	batt_status += 80 
	#if GPIO.input(2) == 1:
	#	batt_status += 60
	#if GPIO.input(3) == 1:
	#	batt_status += 40
	#if GPIO.input(4) == 1:
	#	batt_status += 20

	return battery_status
	

def get_employee_info(rfid = None):
	data = {}
	try:
		instance = Employees.objects.get(employee_rfid=rfid)
		data = {
			"id": instance.id,
			"employee_rfid": instance.employee_rfid,
			"chapa": instance.chapa if instance.chapa else "",
			"employee_id": instance.employee_id
		}

	except MultipleObjectsReturned:
		instance = Employees.objects.filter(employee_rfid=rfid).first()
		data = {
			"id": instance.id,
			"employee_rfid": instance.employee_rfid,
			"chapa": instance.chapa if instance.chapa else "",
			"employee_id": instance.employee_id
		}
		
	except Employees.DoesNotExist:
		data = {}
		
	return data
	
	
		
