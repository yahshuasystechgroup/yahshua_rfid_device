from ..views.common import *

pin_rs = 4
pin_e = 18
pins_db = [24, 21, 20, 16]
RW = 23
def lcd(msg="", msg2=""):
	# pin_rs = 4
	# pin_e = 18
	# pins_db = [24, 21, 20, 16]
	# RW = 23
	
	GPIO.setwarnings(False)
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(pin_e, GPIO.OUT)
	GPIO.setup(pin_rs, GPIO.OUT)
	GPIO.setup(12, GPIO.OUT)
	GPIO.setup(RW, GPIO.OUT)
	for pin in pins_db:
		GPIO.setup(pin, GPIO.OUT)

	clear()
	message(msg)

def clear():
    """ Blank / Reset LCD """

    cmd(0x33) # $33 8-bit mode
    cmd(0x32) # $32 8-bit mode
    #cmd(0x28) # $28 8-bit mode
    #cmd(0x0C) # $0C 8-bit mode
    #cmd(0x06) # $06 8-bit mode
    cmd(0x01) # $01 8-bit mode

def cmd(bits, char_mode=False):
    """ Send command to LCD """
    GPIO.output(RW,0)
    time.sleep(0.001)
    #print(bits)
    bits=bin(bits)[2:].zfill(8)
    #print(bits)

    GPIO.output(pin_rs, char_mode)
    GPIO.output(12, 1)

    for pin in pins_db:
        GPIO.output(pin, False)

    for i in range(4):
        if bits[i] == "1":
            #print(pins_db[::-1][i])
            GPIO.output(pins_db[::-1][i], True)

    GPIO.output(pin_e, True)
    GPIO.output(pin_e, False)

    for pin in pins_db:
        GPIO.output(pin, False)

    for i in range(4,8):
        if bits[i] == "1":
            GPIO.output(pins_db[::-1][i-4], True)

    GPIO.output(pin_e, True)
    GPIO.output(pin_e, False)

def message(text):
    """ Send string to LCD. Newline wraps to second line"""
    for char in text:           
        if char == '\2':
           
            cmd(0x90) # 2nd line 
        elif char == '\n':
            
            cmd(0x88) # 3rd line 
        elif char == '\4':
            
            cmd(0x98) # 4th line 
        else:
            cmd(ord(char),True)
		
        #if char == '\n':
            #cmd(0x0D) # next line
        #else:
            #cmd(ord(char),True)cmd(ord(char),True)
    #for char in text:
        #if char == '\n':
            #cmd(0x0D) # next line
        #else:
            #cmd(ord(char),True)
    #for char in text:
        #cmd(ord(char),True)
           
           
def lcd_picdemo():
    #draw a 16x16 sprite
    #driver.useDisp1()

    #setPage(3)
    #setAddress(2)
    
    #cmd(0x00,1)
    #cmd(0xD0,1)
    #cmd(0x30,1)
    #cmd(0x93,1)
    #cmd(0xDF,1)
    #cmd(0xB7,1)
    #cmd(0x77,1)
    #cmd(0x17,1)
    #cmd(0x17,1)
    #cmd(0x77,1)
    #cmd(0xB7,1)
    #cmd(0xDF,1)
    #cmd(0x33,1)
    #cmd(0xD0,1)
    #cmd(0x10,1)

    #setPage(4)
    #setAddress(0)
    #cmd(0x04,1)
    #cmd(0x0A,1)
    #cmd(0x0B,1)
    #cmd(0x07,1)
    #cmd(0x0F,1)
    #cmd(0xFF,1)
    #cmd(0x71,1)
    #cmd(0x71,1)
    #cmd(0xF1,1)
    #cmd(0xBF,1)
    #cmd(0x7F,1)
    #cmd(0x1F,1)
    #cmd(0x0F,1)
    #cmd(0x17,1)
    #cmd(0x16,1)
    #cmd(0x08,1)
    #print("sssssssssssssssssssssssssSSS")
    
    #cmd(36,1)
    cmd(0x07,1)
    cmd(0x08,1)
    cmd(0x10,1)
    cmd(0x10,1)
    cmd(0x08,1)
    cmd(0x07,1)
    cmd(0x00,1)
    cmd(0x00,1)
    cmd(0xF0,1)
    cmd(0x08,1)
    cmd(0x04,1)
    cmd(0x04,1)
    cmd(0x08,1)
    cmd(0xF0,1)
    cmd(0x00,1)