from ..views.common import *

def error_buzz():
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(26,GPIO.OUT)
	
	do_buzz(0.3, 0.5)
	# do_buzz(0.05, 0.05)
	# do_buzz(0.05, 0.05)
	# do_buzz(0.05, 0.05)
	
	
def do_buzz(self, duration, interval = 0.05, gpio = 26):
	GPIO.output(26, True)
	time.sleep(float(duration))
	GPIO.output(26, False)
	time.sleep(float(interval))