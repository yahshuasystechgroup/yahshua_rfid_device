from django import forms
from ..models.rfid import *

class Time_log_form(forms.ModelForm):
	class Meta:
		model = Time_log
		fields = ('rfid', 'log_type', 'company','date','log_time','img_path','sync_id','device_id', "ip", "employee_id")

class Unregistered_rfid_form(forms.ModelForm):
	class Meta:
		model = Unregistered_rfid
		fields = ('rfid', 'date', 'time','img_path','sync_id','device_id',"ip")

class Employees_form(forms.ModelForm):
	class Meta:
		model = Employees
		fields = ('employee_id', 'employee_name', 'employee_rfid', 'chapa')

class Log_type_setting_form(forms.ModelForm):
	class Meta:
		model = Log_type_setting
		fields = ('is_log_in', 'rfid')

class Timelog_error_form(forms.ModelForm):
	class Meta:
		model = Timelog_error
		fields = ('rfid', 'date', 'time','employee','error_msg')
