from django.apps import AppConfig


class RfidApiConfig(AppConfig):
    name = 'rfid_api'
