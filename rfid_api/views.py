# from django.shortcuts import render

# Create your views here.

from rest_framework import status
from rest_framework import parsers, renderers
# from rest_framework.authtoken.models import Token
# from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User
from rest_framework.decorators import api_view

from rfid_app.models.rfid import *
from rfid_app.views.sync import direct_sync, get_unsynced_data,auto_sync_logs,sync_employees2
from rfid_app.views.battery import battery_charge
#from rfid_app.views.main import battery_indicator

import os
import json

class ObtainAuthToken(APIView):
	throttle_classes = ()
	permission_classes = ()
	parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
	renderer_classes = (renderers.JSONRenderer,)
	# serializer_class = AuthTokenSerializer
	print("RFID API")
	@api_view(['POST'])
	def post(self, request):
		print(request.data)
		return Response()


	# @api_view(['GET'])
	# def get(self, request):
	# 	print("sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
	# 	# print("weeeeeeeeeeeeeeeeeeeee")
	# 	# print(request.data)
	# 	# return Response()
	# 	try:
	# 		data = request.data
	# 		print(data)
	# 		serializer = self.serializer_class(data=data)
	# 		print(serializer)
	# 		print(serializer.is_valid())
	# 		if serializer.is_valid():
	# 			user = serializer.validated_data['user']
	# 			token, created = Token.objects.get_or_create(user=user)

	# 			user_obj = {
	# 				'name': user.fullname,
	# 				'id': user.pk,
	# 				'email': user.email
	# 			}
	# 			response = {'token': token.key, 'user': user_obj}
	# 			return Response(response)
	# 		else:
	# 			print(serializer.errors)
	# 			return Response("Invalid username/password", status=status.HTTP_400_BAD_REQUEST)

	# 	except Exception as e:
	# 		print e
	# 		return Response(e.args, status=status.HTTP_400_BAD_REQUEST)

class LogNotification(APIView):
	def post(self, request, *args, **kwargs):
		unsync_count = get_unsynced_data(return_count = True)
		power_source, batt_status, percentage = battery_charge()
		row = {
				"msg": "SCANNER READY",
				"unsync_count": unsync_count,
				"power_source": power_source,
				"battery_status": batt_status,
				"percentage": percentage				
		}
		try:
			# plug = True
			# battery = False
			# if battery:
			# 	plug
			log_notifications = Log_notification.objects.filter(is_notified=False).order_by("-id")
			if log_notifications.exists():
				notification = log_notifications.first()
				#print(battery_indicator())
				row["msg"] = notification.msg
				row["chapa"] = str(notification.chapa)
				for notification in log_notifications:
					notification.change_status()
					
				return Response(row)
			else:
				return Response(row)
		except Exception as e:
			return Response(row)

class SettingsAccess(APIView):
	def post(self, request, *args, **kwargs):
		try:
			# plug = True
			# battery = False
			# if battery:
			# 	plug
			log_notifications = Special_log_notification.objects.filter(is_notified=False).order_by("-id")

			# result = []
			if log_notifications.exists():
				notification = log_notifications.first()
				row = {
					"has_access": True
				}
				for notification in log_notifications:
					notification.change_status()
					
				return Response(row)
			else:
				return Response({"has_access": False})
		except Exception as e:
			return Response({"has_access": False})

class UpdateLogType(APIView):
	def post(self, request, *args, **kwargs):
		try:
			if (request.data):
				data = request.data
				logs = Special_log_notification.objects.all().order_by("-id")
				log = None
				if logs.exists():
					log = logs.first()

				if not log:
					return Response({
						"success": False, 
						"msg": "INVALID ID!"
					})
				newdata = {
					"is_log_in": data["is_log_in"] == "True",
					"rfid": log.rfid
					# "rfid": data["rfid"],
				}
				Log_type_setting.objects.create(**newdata)
				resp ={
					"success": True, 
					"msg": "LOG TYPE SUCCESFULLY CHANGED."
				}
				return Response(resp)
		except Exception as e:
			raise e

class UpdateRequestStatus(APIView):
	def post(self, request, *args, **kwargs):
		try:
			if (request.data):
				data = request.data
				try:
					request = Request.objects.get(request_type = data.get("type",None))
					is_active = True if data.get("is_active", "False") == "True" else False
					request.change_status(is_active)
					resp ={
						"success": True, 
						"msg": "REQUEST STATUS CHANGED."
					}
					return Response(resp)
				except Request.DoesNotExist:
					resp ={
						"success": False, 
						"msg": "REQUEST SETTING DOES NOT EXIST."
					}
					return Response(resp)
		except Exception as e:
			resp ={
					"success": False, 
					"msg": e
			}
			return Response(resp)
			# raise e

class ReadLogType(APIView):
	def post(self, request, *args, **kwargs):
		try:
			lan_ip = os.popen('ip addr show eth0').read().split("inet ")[1].split("/")[0]
		except Exception as e:
			lan_ip = ""
		
		try:
			wlan_ip = os.popen('ip addr show wlan0').read().split("inet ")[1].split("/")[0]
		except Exception as e:
			wlan_ip = ""
			
		try:
			log_type = Log_type_setting.objects.all().order_by("-id")
			if log_type.exists():
				row = log_type.first()

				resp = {
					"is_log_in": row.is_log_in,
					"lan_ip": lan_ip,
					"wlan_ip": wlan_ip,
				}
				return Response(resp)
		except Exception as e:
			print(e)
			raise e

class DirectSync(APIView):
	def post(self, request, *args, **kwargs):
		return Response(direct_sync())
	# try:
	# 	result = get_unsynced_data(True)
		
	# 	if not result:
	# 		print("%s HAVE NO LOGS TO SYNC! - (%s)"%(DEVICE_ID, datetime.now()))
	# 		return

	# 	# SET HEADERS FOR REQUEST
	# 	headers = {'Authorization':'Token 6747ed5431fbfd27208711483fe6dc4aa2e502c5', 'Content-Type': 'application/json', 'Accept': 'application/json'}
	# 	# headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

	# 	#BATCH SYNCING BY 10 
	# 	data_for_syncing = []
	# 	log_count = 0
	# 	error_data = []
	# 	success_data = []
	# 	for i,res in enumerate(result):
			
	# 		if (i+1) % 10 == 0:
	# 			sync_data = requests.post(LIVE_URL+'/api/sync-time-in-out/', data=json.dumps(data_for_syncing), headers=headers)
	# 			response = json.loads(sync_data._content)
	# 			error_data += response.get("error_data", [])
	# 			success_data = success_data + response.get("success_data", [])
	# 			if sync_data.status_code == 200:
	# 				# set_sync_complete(data_for_syncing)
	# 				log_count += 10
	# 			data_for_syncing = []
	# 		else:
	# 			data_for_syncing.append(res)
	# 			if len(result) == (i + 1):
	# 				sync_data = requests.post(LIVE_URL+'/api/sync-time-in-out/', data=json.dumps(data_for_syncing), headers=headers)
	# 				response = json.loads(sync_data._content)
	# 				error_data += response.get("error_data", [])
	# 				success_data += response.get("success_data", [])
	# 				if sync_data.status_code == 200:
	# 					# set_sync_complete(data_for_syncing)
	# 					log_count += len(data_for_syncing)

	# 	set_sync_complete(success_data, True)
	# 	print(success_data)
	# 	# for synced in success_data:

	# 	print("LOGS SYNC SUCCESSFUL! - (%s - %d log(s) synced)"%(datetime.now(), log_count))
	# except Exception as e:
	# 	# raise ValueError(e)
	# 	return True

class SetDefaultSettings(APIView):
	def post(self, request, *args, **kwargs):
		try:
			if (request.data):
				data = request.data
				result = None
				id_type = ""
				if "id_type" in data:
					id_type = data["id_type"]
				instances = Special_rfid.objects.filter(id_type = id_type)#.values_list("rfid", flat=True)
				if instances.exists():
					result = instances.first()
					if result.rfid: 
						result.rfid = None
						result.save()
				else:
					Special_rfid.objects.create(**{"id_type": str(id_type), "description": str(id_type.upper())})

				if not result:
					resp = {
						"success": False, 
						"msg": "INVALID ID!"
					}
				else:
					resp = {
						"success": True, 
						"rfid": result.rfid
					}
				return Response(resp)
		except Exception as e:
			raise e

class GetDefaultSettings(APIView):
	def post(self, request, *args, **kwargs):
		try:
			instances = Special_rfid.objects.all()#.values_list("rfid", flat=True)
			power = None
			settings = None
			if instances.exists():
				for instance in instances:
					if not power and instance.id_type == "power":
						power = instance.rfid
					if not settings and instance.id_type == "settings":
						settings = instance.rfid
			

			resp = {
				"power_rfid": power,
				"setting_rfid": settings
			}
			return Response(resp)
		except Exception as e:
			raise e

class DefaultSettingsScan(APIView):
	def post(self, request, *args, **kwargs):
		try:
			count = Special_rfid.objects.filter(rfid__isnull = True).count()#.values_list("rfid", flat=True)
			resp = {
				"exit": count == 0,
			}
			return Response(resp)
		except Exception as e:
			raise e		

class SyncLogs(APIView):
	def post(self, request, *args, **kwargs):
		return Response(auto_sync_logs())


#Triggered update by local server (settings)
class Update_device_settings(APIView):
	def post(self, request, *args, **kwargs):
		return Response(sync_employees2(True))
