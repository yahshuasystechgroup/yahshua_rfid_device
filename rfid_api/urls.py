from rfid_api import views
from django.conf.urls import url, include

urlpatterns = [
	url(r'^api-auth/', views.ObtainAuthToken.as_view()),
	url(r'^get-log-notification/', views.LogNotification.as_view()),
	url(r'^get-settings-access/', views.SettingsAccess.as_view()),
	url(r'^update-log-type/', views.UpdateLogType.as_view()),
	url(r'^update-request-status/', views.UpdateRequestStatus.as_view()),
	url(r'^read-log-type/', views.ReadLogType.as_view()),
	url(r'^manual-sync/', views.DirectSync.as_view()),
	url(r'^sync-logs/', views.SyncLogs.as_view()),
	#url(r'^print-special-rfid/', views.PrintSpecialRFID.as_view()),
	url(r'^set-default-settings/', views.SetDefaultSettings.as_view()),
	url(r'^get-default-settings/', views.GetDefaultSettings.as_view()),
	url(r'^default-settings-scan/', views.DefaultSettingsScan.as_view()),
]
