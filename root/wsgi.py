"""
WSGI config for root project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "root.settings")

application = get_wsgi_application()

#To call python manage.py rfid along with runserver

# from django.core import management
# from django.core.management.commands import loaddata

# management.call_command('rfid', verbosity=0, interactive=False)