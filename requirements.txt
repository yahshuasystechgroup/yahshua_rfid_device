#Django==1.9
django==1.9
psycopg2

# INSTALL SPI-PY
# git clone http://github.com/lthiery/SPI-PY.git
# cd SPI-Py
# python setup.py install

requests
djangorestframework==3.3.3